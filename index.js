
const objectInpunt = {

}

function createDeepCopy(object) {
  
  if (typeof object !== "object") {
    return object;
  }
  if (object instanceof Date) {
    return new Date(object.getTime());
  }
  let copy = Array.isArray(object) ? [] : {};
  for (let key in object) {
    const value = object[key];
    copy[key] = createDeepCopy(value);
  }

  return copy;
};

const copyObject = createDeepCopy(objectInpunt)
console.log(copyObject)
